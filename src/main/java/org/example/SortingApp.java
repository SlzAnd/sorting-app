package org.example;

import java.io.InputStream;
import java.io.ObjectStreamException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 *This class take up to 10 integer values entered by command line, sort in ASC order and print out
 */
public class SortingApp {
    final InputStream stream;

    public SortingApp() {
        this.stream = System.in;
    }

    public SortingApp(InputStream inputStream) {
        this.stream = inputStream;
    }

    public void sorting() {

        List<Integer> enteredValues = new ArrayList<>();
        Scanner scanner = new Scanner(this.stream);

        String values = scanner.nextLine();
        String[] arr = values.split(" ");
        if (arr.length > 0 && arr.length < 10)
            for(String str : arr)
                enteredValues.add(Integer.parseInt(str));
        else throw new IllegalArgumentException("up to 10 integer values");

        scanner.close();

        // sort values
        List<Integer> sortedEnteredValues = enteredValues.stream()
                .sorted(Integer::compareTo)
                .collect(Collectors.toList());

        // print sorted entered values
        sortedEnteredValues.forEach(System.out::println);
    }

    public static void main( String[] args ) {

    }
}
