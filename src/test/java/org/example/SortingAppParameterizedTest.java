package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingAppParameterizedTest {

    private final String input;
    private final List<Integer> expectedOutput;

    public SortingAppParameterizedTest(String input, List<Integer> expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testData() {
        return Arrays.asList(new Object[][]{
                {"5 4 1 8 7 6 2 3 10", Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 10)}, // top bound
                {"1 1 1 1 1 1 1 1", Arrays.asList(1, 1, 1, 1, 1, 1, 1, 1)},         // top bound -1
                {"10 9 8 7 6", Arrays.asList(6, 7, 8, 9, 10)},                      // middle
                {"0", Arrays.asList(0)},
                {"9 1", Arrays.asList(1, 9)}
        });
    }
    /**
    *  Equivalence and Boundary Values Test
     **/
    @Test
    public void testSorting() {
        // prepare input stream
        InputStream inputStream = new ByteArrayInputStream(input.getBytes());

        // create SortingApp instance
        SortingApp sortingApp = new SortingApp(inputStream);

        // call sorting method
        sortingApp.sorting();

        // verify output
        List<Integer> sortedOutput = getSortedOutput(input);
        assertEquals(expectedOutput, sortedOutput);
    }



    private List<Integer> getSortedOutput(String input) {
        // prepare input stream
        InputStream inputStream = new ByteArrayInputStream(input.getBytes());

        // create SortingApp instance
        SortingApp sortingApp = new SortingApp(inputStream);

        // call sorting method and capture output
        final StringBuffer buffer = new StringBuffer();
        System.setOut(new java.io.PrintStream(new java.io.OutputStream() {
            @Override
            public void write(int b) {
                buffer.append((char) b);
            }
        }));
        sortingApp.sorting();

        // parse output
        String output = buffer.toString();
        List<Integer> sortedOutput = Arrays.asList(output.split("\\r?\\n")).stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        return sortedOutput;
    }
}
