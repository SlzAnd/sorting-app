package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class SortingAppOutOfBoundariesParametrizedTest {

    final String input;

    public SortingAppOutOfBoundariesParametrizedTest(String input) {
        this.input = input;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {" "},
                {"1 1 1 1 1 1 1 1 1 1 1"}
        });
    }

    @Test (expected = IllegalArgumentException.class)
    public void testOutOfBoundaries() {
        InputStream inputStream = new ByteArrayInputStream(input.getBytes());

        SortingApp sortingApp = new SortingApp(inputStream);

        sortingApp.sorting();
    }
}
